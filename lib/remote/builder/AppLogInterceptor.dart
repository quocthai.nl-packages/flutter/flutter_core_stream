import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:logger/logger.dart';

import '../../sw_core_package.dart';

class AppLogInterceptor extends InterceptorsWrapper {
  final Logger logger = Logger(printer: SimpleLogPrinter('RequestBuilder'));
  int maxCharactersPerLine = 300;

  Future<Map<String, dynamic>> Function() buildHeader;

  AppLogInterceptor(this.buildHeader);

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers = await buildHeader();
    Map<String, dynamic>? requestParams = options.queryParameters;
    var data = options.data;
    logger.v("Start Request");

    logger.v("queryParameters: ${jsonEncode(requestParams)}");
    if (data != null) {
      if (data is FormData) {
        logger.v('Form data');
        if (data.fields.isNotEmpty == true) {
          data.fields.forEach((entry) {
            logger.v('Key: ${entry.key} - ${entry.value}');
          });
        }
        if (data.files.isNotEmpty == true) {
          data.files.forEach((entry) {
            logger.v(
                'filename: ${entry.value.filename} - contentType: ${entry.value
                    .contentType.toString()} - length: ${entry.value.length}');
          });
        }
      } else {
        logger.v('data: $data');
        logger.v('size: ${utf8
            .encode(data.toString())
            .length}');
      }
    }
    logger.v("path: ${options.uri.toString()}");
    logger.v("method: ${options.method.toString()}\n");
    logger.v("header: ${options.headers}");
    options.contentType = Headers.jsonContentType;
    logger.v("Content type: ${options.contentType}\n");
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    logger.i(
        "Response: code - ${response.statusCode} for method - ${response
            .requestOptions.method.toString()}, and path - ${response
            .requestOptions
            .baseUrl + response.requestOptions.path}");
    String responseAsString = response.data.toString();
    if (responseAsString.length > maxCharactersPerLine) {
      int iterations =
      (responseAsString.length / maxCharactersPerLine).floor();
      for (int i = 0; i <= iterations; i++) {
        int endingIndex = i * maxCharactersPerLine + maxCharactersPerLine;
        if (endingIndex > responseAsString.length) {
          endingIndex = responseAsString.length;
        }
        logger.i(responseAsString.substring(
            i * maxCharactersPerLine, endingIndex));
      }
    } else {
      logger.i("Response data: ${response.data}");
    }
    logger.d("End Request\n\n");
    return handler.next(response); // cont
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    logger.v('*** DioError ***:');
    logger.v('uri: ${err.requestOptions.uri}');
    logger.v('$err');
    if (err.response != null) {
      _printResponse(err.response!);
    }
    handler.next(err);
  }

  void _printResponse(Response response) {
    _printKV('uri', response.requestOptions.uri);
    _printKV('statusCode', response.statusCode);
    if (response.isRedirect == true) {
      _printKV('redirect', response.realUri);
    }

    logPrint('headers:');
    response.headers.forEach((key, v) => _printKV(' $key', v.join('\r\n\t')));
    logPrint('Response Text:');
    _printAll(response.toString());
    logPrint('');
  }

  void _printKV(String key, Object? v) {
    logPrint('$key: $v');
  }

  void logPrint(String log) {
    logger.v(log);
  }

  void _printAll(msg) {
    msg.toString().split('\n').forEach(logPrint);
  }
}

