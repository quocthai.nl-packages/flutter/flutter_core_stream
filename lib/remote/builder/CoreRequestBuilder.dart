import 'dart:io';
import 'dart:math';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:sw_core_package/data/models/base/CoreResponse.dart';

//import 'package:logging/logging.dart';
import 'package:sw_core_package/data/models/error/CoreResponseError.dart';
import 'package:sw_core_package/remote/builder/AppLogInterceptor.dart';
import 'package:sw_core_package/remote/builder/AuthorizationInterceptor.dart';
import 'package:sw_core_package/remote/constants/RemoteConstants.dart';
import 'package:sw_core_package/sw_core_package.dart';
import 'package:sw_core_package/utilities/CoreStorageManager.dart';
import 'package:sw_core_package/utilities/CoreStringUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:logger/logger.dart';
import 'package:sw_core_package/utilities/rxbus/CoreBusMessages.dart';
import 'package:sw_core_package/utilities/rxbus/rxbus.dart';
import 'dart:convert' as convert;

class SimpleLogPrinter extends LogPrinter {
  final String className;

  SimpleLogPrinter(this.className);

  @override
  List<String> log(LogEvent event) {
    return [event.message];
  }
}

abstract class CoreRequestBuilder {
  Dio? _dio;
  Dio? _refreshDio;

  CoreRequestBuilder([Dio? dio, Dio? refreshDio]) {
    recreateDio(dio, refreshDio);
  }

  void recreateDio([Dio? dio, Dio? refreshDio]) {
    if (dio == null) {
      _dio = Dio(buildRequestOptions());
    } else {
      _dio = dio;
    }
    if (_refreshDio == null) {
      _refreshDio = Dio(buildRequestOptions());
    } else {
      _refreshDio = refreshDio;
    }
    setupInterceptors();
  }

//  final Logger _logger = new Logger("RequestBuilder");
  final Logger logger = Logger(printer: SimpleLogPrinter('RequestBuilder'));

  String get refreshTokenEndPoint => "access/refresh-token";

  String get accessTokenParamKey => 'accessToken';

  String get refreshTokenParamKey => 'refreshToken';

  String get headerTokenParamKey => 'x-access-token';

  Future<void> performRequest(
      String endPoint,
      String httpMethod,
      Map<String, dynamic>? requestParams,
      Function(Map<String, dynamic> responseData) onResponse,
      dynamic Function(CoreResponseError error) onError) async {
    try {
      Response? response;
      switch (httpMethod) {
        case CoreHTTPMethod.get:
          response = await _dio?.get(endPoint, queryParameters: requestParams);
          break;
        case CoreHTTPMethod.post:
          response = await _dio?.post(endPoint, data: requestParams);
          break;
        case CoreHTTPMethod.put:
          response = await _dio?.put(endPoint, data: requestParams);
          break;
        case CoreHTTPMethod.patch:
          response = await _dio?.patch(endPoint, data: requestParams);
          break;
        case CoreHTTPMethod.delete:
          response = await _dio?.delete(endPoint, data: requestParams);
          break;
        default:
          response = await _dio?.patch(endPoint, data: requestParams);
      }
      // we should have header code as 200 here
      await onResponse(response?.data);
    } on DioError catch (error) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      var errorResponse = handleResponseError(error);
      if (errorResponse != null) {
        await onError(errorResponse);
      }
    }
  }

  Future<void> uploadImage(
      String endPoint,
      FormData formData,
      Function(Map<String, dynamic> responseData) onResponse,
      dynamic Function(CoreResponseError error) onError) async {
    try {
      //Call refresh token first to avoid error when upload with dio
      await refreshToken();
      Response? response;
      response = await _dio?.put(endPoint, data: formData);
      // we should have header code as 200 here
      onResponse(response?.data);
    } on DioError catch (error) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      var errorResponse = handleResponseError(error);
      if (errorResponse != null) {
        onError(errorResponse);
      }
    }
  }

  Future<Map<String, dynamic>> buildHeader() async {
    var headers = Map<String, dynamic>();
    final prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(CoreStorageManager.userTokenKey);
    if (CoreStringUtils.isNotEmpty(token)) {
      headers[headerTokenParamKey] = token;
      logger.d("$headerTokenParamKey: $token");
    }
    return headers;
  }

  BaseOptions buildRequestOptions() {
    return BaseOptions(
      baseUrl: CoreRemoteConstants.baseUrl,
      contentType: Headers.formUrlEncodedContentType,
      responseType: ResponseType.json,
      connectTimeout: 30000,
      receiveTimeout: 30000,
    );
  }

  AuthorizationInterceptor refreshTokenInterceptor() {
    return AuthorizationInterceptor(
        rootDio: _dio,
        refreshDio: _refreshDio,
        refreshTokenEndPoint: refreshTokenEndPoint,
        headerTokenParamKey: headerTokenParamKey,
        accessTokenParamKey: accessTokenParamKey,
        refreshTokenParamKey: refreshTokenParamKey,
        getToken: () async {
          await provideStorageManager().initPrefs();
          return provideStorageManager().getToken ?? '';
        },
        getRefreshToken: () async {
          return provideStorageManager().getRefreshToken ?? '';
        },
        saveToken: (token) {
          return provideStorageManager().setToken(token);
        },
        saveRefreshToken: (newRefreshToken) {
          return provideStorageManager().setRefreshToken(newRefreshToken);
        });
  }

  void setupInterceptors() {
    _setUpCertificatePining(_dio);
    _setUpCertificatePining(_refreshDio);
    // setup request

    _dio?.interceptors.add(AppLogInterceptor(buildHeader));
    _refreshDio?.interceptors.add(AppLogInterceptor(buildHeader));

    _dio?.interceptors.add(refreshTokenInterceptor());
    _refreshDio?.interceptors.add(refreshTokenInterceptor());
    // _dio?.interceptors.add(InterceptorsWrapper(
    //     onRequest:(options, handler) async {
    //       options.headers = await buildHeader();
    //         logger.v("Start Request");
    //         if (requestParams != null) {
    //           logger.v("requestParams: $requestParams");
    //         }
    //         if (formData != null) {
    //           logger.v("formData: $formData");
    //         }
    //         logger.v("path: ${CoreRemoteConstants.baseUrl}$endPoint");
    //         logger.v("method: ${options.method.toString()}\n");
    //         logger.v("header: ${options.headers}");
    //         options.contentType = Headers.jsonContentType;
    //         logger.v("Content type: ${options.contentType}\n");
    //       return handler.next(options); //continue
    //     },
    //     // setup response
    //     onResponse:(response,handler) {
    //       logger.i(
    //           "Response: code - ${response.statusCode} for method - ${response
    //               .requestOptions.method.toString()}, and path - ${response.requestOptions
    //               .baseUrl + response.requestOptions.path}");
    //       String responseAsString = response.data.toString();
    //       if (responseAsString.length > maxCharactersPerLine) {
    //         int iterations =
    //         (responseAsString.length / maxCharactersPerLine).floor();
    //         for (int i = 0; i <= iterations; i++) {
    //           int endingIndex = i * maxCharactersPerLine + maxCharactersPerLine;
    //           if (endingIndex > responseAsString.length) {
    //             endingIndex = responseAsString.length;
    //           }
    //           logger.i(responseAsString.substring(
    //               i * maxCharactersPerLine, endingIndex));
    //         }
    //       } else {
    //         logger.i("Ressponse data: ${response.data}");
    //       }
    //       logger.d("End Request\n\n");
    //       return handler.next(response); // continue
    //     },
    //     // setup errors
    //     onError: (DioError error, handler) async {
    //       logger.e("Request got error");
    //       // The request was made and the server responded with a status code
    //       // that falls out of the range of 2xx and is also not 304.
    //       if (error.response != null) {
    //         logger.e("${error.response}\n\n");
    //         if (error.response?.statusCode == CoreHttpCode.TOKEN_INVALID) {//RxBusTag.RxBusTag_TOKEN_INVALID
    //           return handler.next(error);
    //         }
    //         if (error.response?.statusCode == CoreHttpCode.TOKEN_EXPIRED) {
    //           RequestOptions? options = error.response?.requestOptions;
    //
    //           //Lock request and response lock
    //           // _dio?.interceptors.requestLock.lock();
    //           // _dio?.interceptors.responseLock.lock();
    //
    //           try {
    //             String newToken = await refreshToken();
    //             //Release lock
    //             _dio?.interceptors.requestLock.unlock();
    //             _dio?.interceptors.responseLock.unlock();
    //
    //             options?.headers[headerTokenParamKey] = newToken;
    //             var response = await _dio?.request(options?.path ?? '',
    //              options: Options(method: options?.method ?? ''),
    //                 data: options?.data,
    //                 queryParameters: options?.queryParameters,
    //                 onSendProgress: options?.onSendProgress,
    //                 onReceiveProgress: options?.onReceiveProgress
    //             );
    //             if (response != null)
    //               return handler.resolve(response);
    //             else
    //               return handler.reject(error);
    //           } on DioError catch (dioError) {
    //             //Release lock
    //             _dio?.interceptors.requestLock.unlock();
    //             _dio?.interceptors.responseLock.unlock();
    //
    //             return handler.next(dioError);
    //           }
    //         }
    //       } else {
    //         // Something happened in setting up or sending the request that triggered an Error
    //         // logger.e(error.requestOptions);
    //         logger.e("${error.message}\n\n");
    //       }
    //       logger.d("\n\n");
    //       return handler.next(error);//continue
    //       // If you want to resolve the request with some custom data，
    //       // you can resolve a `Response` object eg: `handler.resolve(response)`.
    //     }
    // ));

    var interceptors = interceptorList() ?? <Interceptor>[];
    if (interceptors.isNotEmpty) {
      _dio?.interceptors.addAll(interceptors);
      _refreshDio?.interceptors.addAll(interceptors);
    }
  }

  // //Call refresh token
  Future<String> refreshToken() async {
    Map<String, dynamic> refreshTokenRequestParams = Map<String, dynamic>();
    await provideStorageManager().initPrefs();
    var token = provideStorageManager().getToken;
    var refreshToken = provideStorageManager().getRefreshToken;
    refreshTokenRequestParams[accessTokenParamKey] = token;
    refreshTokenRequestParams[refreshTokenParamKey] = refreshToken;

    Response? response =
        await _dio?.post(refreshTokenEndPoint, data: refreshTokenRequestParams);

    final newAccessToken = response?.data["data"][accessTokenParamKey];
    final newRefreshToken = response?.data["data"][refreshTokenParamKey];

    await provideStorageManager().setToken(newAccessToken);
    await provideStorageManager().setRefreshToken(newRefreshToken);
    return newAccessToken;
  }

  ///Abstract method
  void _setUpCertificatePining(Dio? providedDio) async {
    try {
      String pemFile = await providePemFile();
      if (CoreStringUtils.isNotEmpty(pemFile)) {
        (providedDio?.httpClientAdapter as DefaultHttpClientAdapter)
            .onHttpClientCreate = (client) {
          SecurityContext securityContext = SecurityContext();
          securityContext.setTrustedCertificatesBytes(pemFile.codeUnits);
          var certificatedClient = HttpClient(context: securityContext);
          return certificatedClient;
        };
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<String> providePemFile() {
    throw UnimplementedError();
  }

  CoreStorageManager provideStorageManager();

  List<Interceptor>? interceptorList() {
    return [];
  }

  CoreResponseError? handleResponseError(DioError error) {
    String errorMessage = "";
    int errorCode = 0;
    try {
      if (
          //error.type == DioErrorType.DEFAULT ||
          error.type == DioErrorType.connectTimeout ||
              error.type == DioErrorType.receiveTimeout ||
              error.type == DioErrorType.sendTimeout) {
        errorCode = 1001; // No connection
      } else {
        if (error.response != null) {
          errorCode = error.response?.statusCode ?? 0;
          if (error.response?.data != null) {
            errorMessage = retrieveErrorMessage(error.response?.data);
          }
        } else {
          errorMessage = error.message;
        }
      }
      return CoreResponseError.fromValues(errorCode, errorMessage);
    } catch (error) {
      return CoreResponseError.fromValues(errorCode, errorMessage);
    }
  }

  String retrieveErrorMessage(Map<String, dynamic> jsonObject);
}
