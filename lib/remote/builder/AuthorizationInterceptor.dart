import 'package:dio/dio.dart';
import 'package:sw_core_package/remote/builder/JwtDecoder.dart';
import 'package:sw_core_package/remote/constants/RemoteConstants.dart';

class AuthorizationInterceptor extends InterceptorsWrapper {
  final Dio? refreshDio;
  final Dio? rootDio;
  final String refreshTokenEndPoint;
  final String headerTokenParamKey;
  final String accessTokenParamKey;
  final String refreshTokenParamKey;

  final Future<String> Function() getToken;
  final Future<String> Function() getRefreshToken;
  final Future<void> Function(String)? saveToken;
  final Future<void> Function(String)? saveRefreshToken;

  AuthorizationInterceptor({
    this.rootDio,
    this.refreshDio,
    required this.refreshTokenEndPoint,
    required this.headerTokenParamKey,
    required this.accessTokenParamKey,
    required this.refreshTokenParamKey,
    required this.getToken,
    required this.getRefreshToken,
    this.saveToken,
    this.saveRefreshToken,
  });

  bool _isRefreshedToken = true;

  // @override
  // void onRequest(RequestOptions options,
  //     RequestInterceptorHandler handler) async {
  //   var token = options.headers[headerTokenParamKey]?.toString() ?? '';
  //   if (token.isNotEmpty
  //       && JwtDecoder.isExpired(token)
  //       && options.path != refreshTokenEndPoint) {
  //     rootDio?.lock();
  //     try {
  //       String newToken = await refreshToken();
  //       options.headers[headerTokenParamKey] = newToken;
  //       rootDio?.unlock();
  //     } on DioError catch (dioError) {
  //       return handler.reject(dioError, true);
  //     }
  //   }
  //   super.onRequest(options, handler);
  // }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    super.onResponse(response, handler);
  }

  @override
  void onError(DioError error, ErrorInterceptorHandler handler) async {
    if (error.response != null) {
      if (error.response?.statusCode == CoreHttpCode.TOKEN_INVALID) {
        return handler.next(error);
      }
      if (error.response?.statusCode == CoreHttpCode.TOKEN_EXPIRED) {
        ///Lock request and response lock
        _lock();
        RequestOptions? options = error.response?.requestOptions;
        String currentToken = await getToken();
        String headerToken = options?.headers[headerTokenParamKey] ?? '';
        if (headerToken != currentToken) {
          _unlock();
          return _makeNewRequest(options, handler);
        }
        try {
          String newToken = await refreshToken();
          ///Release lock
          _unlock();
          options?.headers[headerTokenParamKey] = newToken;
          return _makeNewRequest(options, handler);
        } on DioError catch (dioError) {
          ///Release lock
          _unlock();
          return handler.next(dioError);
        }
      }
    }
    handler.next(error);
  }

  void _makeNewRequest(RequestOptions? options,
      ErrorInterceptorHandler handler) async {
    try {
      var response = await rootDio?.request(options?.path ?? '',
          options: Options(
            method: options?.method ?? '',
            receiveTimeout: options?.receiveTimeout,
            sendTimeout: options?.sendTimeout,
            extra: options?.extra,
            headers: options?.headers,
            responseType: options?.responseType,
            validateStatus: options?.validateStatus,
            receiveDataWhenStatusError: options?.receiveDataWhenStatusError,
            followRedirects: options?.followRedirects,
            maxRedirects: options?.maxRedirects,
            requestEncoder: options?.requestEncoder,
            responseDecoder: options?.responseDecoder,
            listFormat: options?.listFormat,
          ),
          data: options?.data,
          queryParameters: options?.queryParameters,
          onSendProgress: options?.onSendProgress,
          onReceiveProgress: options?.onReceiveProgress);
      return handler.resolve(response!);
    } on DioError catch (dioError) {
      handler.reject(dioError);
    }
  }

  void _lock() {
    rootDio?.interceptors.requestLock.lock();
    rootDio?.interceptors.responseLock.lock();
    rootDio?.interceptors.errorLock.lock();
  }

  void _unlock() {
    rootDio?.interceptors.requestLock.unlock();
    rootDio?.interceptors.responseLock.unlock();
    rootDio?.interceptors.errorLock.unlock();
  }

  //Call refresh token
  Future<String> refreshToken() async {
    Map<String, dynamic> refreshTokenRequestParams = Map<String, dynamic>();
    var token = await getToken();
    var refreshToken = await getRefreshToken();
    refreshTokenRequestParams[accessTokenParamKey] = token;
    refreshTokenRequestParams[refreshTokenParamKey] = refreshToken;

    Response? response =
    await refreshDio?.post(
        refreshTokenEndPoint, data: refreshTokenRequestParams);

    final newAccessToken = response?.data["data"][accessTokenParamKey];
    final newRefreshToken = response?.data["data"][refreshTokenParamKey];
    await saveToken?.call(newAccessToken);
    await saveRefreshToken?.call(newRefreshToken);
    return newAccessToken;
  }
}
