import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

abstract class CoreStorageManager {

  SharedPreferences? prefs;

  Future<void> initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<void> setToken(String? token);

  String? get getToken;

  Future<void> setRefreshToken(String? token);

  String? get getRefreshToken;

  //// Keys
  static const userTokenKey = "_userTokenKey";
  static const refreshTokenKey = "_refreshTokenKeyy";

}