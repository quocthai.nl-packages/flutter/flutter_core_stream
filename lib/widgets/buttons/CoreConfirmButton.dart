import 'package:flutter/material.dart';
import 'package:sw_core_package/sw_core_package.dart';
import 'package:sw_core_package/widgets/buttons/bases/CoreStatelessButton.dart';

class CoreConfirmButton extends CoreStatelessButton {
  CoreConfirmButton({String? text, Function()? onTapCallback,
      EdgeInsetsGeometry buttonMargin = const EdgeInsets.only(
          top: CoreDimens.defaultPadding, bottom: CoreDimens.defaultPadding),
      bool isEnabled = true,
      int height = 50,
      List<Color> arrayColors = const []})
      : super(
            text ?? '', onTapCallback ?? (){}, buttonMargin, isEnabled, height, arrayColors);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (isEnabled ?? false) ? onTapCallback : null,
      child: Container(
        margin: buttonMargin,
        height: CoreDimens.dp_50,
        alignment: FractionalOffset.center,
        decoration: new BoxDecoration(
            color: (isEnabled ?? false)
                ? CoreColors.buttonEnableBackgroundColor
                : CoreColors.buttonDisableBackgroundColor,
            borderRadius: new BorderRadius.all(
                const Radius.circular(CoreDimens.defaultRadius)),
            gradient: ((isEnabled ?? false) && (arrayColors?.length ?? 0) >= 2)
                ? LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft, colors: arrayColors ?? [])// TODO: colors: arrayColors
                : null),
        child: new Text(
          (buttonText ?? '').toUpperCase(),
          style: new TextStyle(
            color: (isEnabled ?? false)
                ? CoreColors.buttonEnableTextColor
                : CoreColors.buttonDisableTextColor,
            fontSize: CoreDimens.SemiLargeTextSize,
            fontWeight: FontWeight.w600,
            letterSpacing: 0.3,
          ),
        ),
      ),
    );
  }
}
